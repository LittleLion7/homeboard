import sys
from PyQt5.QtWidgets import *
import pandas as pd
import plotly.express as px
from datetime import date
import webbrowser
import matplotlib.pyplot as plt


#get actual day
today = date.today()
today = today.strftime("%d.%m.%Y")

app = QApplication(sys.argv)
widget = QWidget()
label = QLabel("", widget)
labelx = QLabel("", widget)
label2 = QLabel("", widget)
label3 = QLabel("", widget)
label4 = QLabel("", widget)
label5 = QLabel("", widget)
 
btnAdd = QPushButton("Zapisz", widget) 
 
#txtArea = QPlainTextEdit("Text To Edit", widget)widget.resize
 
txtArea1 = QLineEdit("", widget)
txtArea2 = QLineEdit("", widget)
txtArea3 = QLineEdit("", widget)
txtArea4 = QLineEdit("", widget)
 
LabelFixedWidth = 150

def init():
 
    widget.resize(360, 310)
    widget.move(500, 500)
    widget.setWindowTitle('Domowe statystyki')
    widget.show()
 
    label.setFixedWidth(300)
    label.setText(today)
    label.move(20,5)
    label.show()
    
    labelx.setFixedWidth(350)
    labelx.setText("Witajcie kochani, podajcie prosze wasze wyniki:")
    labelx.move(20,20)
    labelx.show()
    
    label2.setFixedWidth(LabelFixedWidth)
    label2.setText("Cukier Omi")
    label2.move(20,40)
    label2.show()
    txtArea1.move(200,40)
    txtArea1.show()

    label3.setFixedWidth(LabelFixedWidth)
    label3.setText("Waga Omi")
    label3.move(20,70)
    label3.show()
    txtArea2.move(200,70)
    txtArea2.show()
 
    label4.setFixedWidth(LabelFixedWidth)
    label4.setText("Cisnienie Mama")
    label4.move(20,100)
    label4.show()
    txtArea3.move(200,100)
    txtArea3.show()

    label5.setFixedWidth(LabelFixedWidth)
    label5.setText("Wypalone papierosy tata")
    label5.move(20,130)
    label5.show()
    txtArea4.move(200,130)
    txtArea4.show()
 
    btnAdd.setToolTip('Zapisz')
    btnAdd.move(20,250)
    btnAdd.clicked.connect(save)
    btnAdd.show()
 
 
def save():
    if txtArea1.text():  
        df = pd.read_csv('OmiCukier.csv')
        df.loc[len(df.index)] = [today, int(txtArea1.text())]  
        df.to_csv('OmiCukier.csv', index = False)
        fig = df.plot().get_figure()
        fig.savefig('FotoFrame/OmiCukier.jpg')

    if txtArea2.text():
        df = pd.read_csv('OmiWaga.csv')
        df.loc[len(df.index)] = [today, int(txtArea2.text())]  
        df.to_csv('OmiWaga.csv', index = False)
        fig2 = df.plot().get_figure()
        fig2.savefig('FotoFrame/OmiWaga.jpg')
        
    if txtArea3.text():
        df = pd.read_csv('MamaCisnienie.csv')
        df.loc[len(df.index)] = [today, int(txtArea3.text())]  
        df.to_csv('MamaCisnienie.csv', index = False)
        fig = df.plot().get_figure()
        fig.savefig('FotoFrame/MamaCisnienie.jpg')
        
    if txtArea4.text():
        df = pd.read_csv('TataPapierosy.csv')
        df.loc[len(df.index)] = [today, int(txtArea4.text())]  
        df.to_csv('TataPapierosy.csv', index = False)
        fig = df.plot().get_figure()
        fig.savefig('FotoFrame/TataPapierosy.jpg')
        #a_website = "file:///C:/sources/homeboard/FotoFrame/index.html"
        a_website = "file:///home/pi/sources/homeboard/FotoFrame/index.html"
        #a_website = "file:///home/patryk/sources/homeboard/FotoFrame/index.html"
        webbrowser.open_new(a_website)

    qApp.exit();
 
if __name__ == "__main__":
     init()
 
app.exec_()
